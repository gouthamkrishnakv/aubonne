# `aubonne`

This project is my tutorial to `SDL2`.

Tutorial is from [LazyFoo' Productions](https://lazyfoo.net/), found [here](https://lazyfoo.net/tutorials/SDL/)

## Requirements

### Ubuntu
```bash
sudo apt-get install libsdl2-dev meson ninja
```
### Fedora 29+ / CentOS/RHEL 8+
```bash
sudo dnf install SDL2-devel meson ninja
```

### CentOS/RHEL (below 8)
```bash
sudo yum install SDL2-devel meson ninja
```
### Pacman/AUR
```bash
pacman -Sy sdl2-2.0 meson
```

## Building

### Compiling
- Setup before compilation
  ```bash
  meson setup debug
  ```
- Compile
  ```bash
  meson compile -C debug
  ```
### Executing
- Execute (MacOS/Linux)
  ```bash
  ./debug/aubonne
  ```
- Execute Windows
  ```bash
  ./debug/aubonne.exe
  ```

## License

**IMPORTANT: Use it at your own discretion. This project comes with warranties and liabilities as described by MIT License. I'm not liable for any responsibility other that what the license describes.**

[MIT License](LICENSE)

Copyright (C) 2020 Goutham Krishna K V