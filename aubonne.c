/**
 *  aubonne
 *      My Tutorial of SDL2
 *  This project is licensed with MIT License,
 *  if not recieved with this, please assume so.
 * 
 *  LIABILITIES AND WARRANTY COVERED BY MIT LICENSE
 *  read more in README.md
*/

#include <stdbool.h>
#include <SDL.h>

#define SCREEN_WIDTH 640
#define SCREEN_HEIGHT 480

bool init(SDL_Window **window, SDL_Surface **surface)
{
    // Success return value
    bool success = true;

    // Initialize SDL
    if (SDL_Init(SDL_INIT_VIDEO) < 0)
    {
        printf("SDL could not initialize: SDL_Error: %s", SDL_GetError());
        success = false;
    }
    else
    {
        *window = SDL_CreateWindow(
            "SDL_Tutorial",
            SDL_WINDOWPOS_UNDEFINED,
            SDL_WINDOWPOS_UNDEFINED,
            SCREEN_WIDTH,
            SCREEN_HEIGHT,
            SDL_WINDOW_SHOWN);
        if (window == NULL)
        {
            printf("Window could not be created: SDL_Error: %s", SDL_GetError());
            success = false;
        }
        else
        {
            *surface = SDL_GetWindowSurface(*window);
        }
    }

    return success;
}

bool load_media(SDL_Surface **surface)
{
    bool success = true;

    // load helloworld.bmp file
    *surface = SDL_LoadBMP("./res/hello_world.bmp");
    // if helloworld is not null
    if (*surface == NULL)
    {
        // print error
        printf("Unable to load image: SDL_Error: %s", SDL_GetError());
        // update success variable
        success = false;
    }
    // return success variable
    return success;
}

int main(
    // int argc,
    // char **argv
)
{
    // Window we'll be rendering to
    SDL_Window *window = NULL;

    // Surface contained by the window
    SDL_Surface *screen_surface = NULL;

    // create window
    if (init(&window, &screen_surface))
    {
        // define a helloworld surface
        SDL_Surface *helloworld_surface = NULL;
        // load the bitmap media
        if (load_media(&helloworld_surface))
        {
            // show it to window
            SDL_BlitSurface(helloworld_surface, NULL, screen_surface, NULL);
            // update window
            SDL_UpdateWindowSurface(window);
            // wait 2 seconds
            SDL_Delay(2000);
            // free the bmp surface
            SDL_FreeSurface(helloworld_surface);
            helloworld_surface = NULL;

            // destroy window
            // SDL_DestroyWindow(window);
            // window = NULL;
        }
        else
        {
            printf("FAILED TO GET MEDIA\n");
            return (2);
        }
    }
    else
    {
        printf("PROGRAM EXITING");
        return (1);
    }

    SDL_Quit();
    // return a success error message
    return (0);
}
